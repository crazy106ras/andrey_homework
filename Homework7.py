# 1. Написать функцию которой передается один параметр - список строк my_list.
# Функция возвращает новый список в котором содержаться
# элементы из my_list по следующему правилу:
# Если строка стоит на нечетном месте в my_list, то ее заменить на
# перевернутую строку. "qwe" на "ewq".
# Если на четном - оставить без изменения.

def my_def(value):
    new_list = []
    for i in value:
        if not value.index(i) % 2:
            new_list.append(i)
        else:
            new_list.append(str(i)[::-1])
    return(new_list)


my_list = ['asd', 'asdaf', 'febe', 'adfw', '6546', 46]
print(my_def(my_list))

############################################################################################

# 2. Написать функцию которой передается один параметр - список строк my_list.
# Функция возвращает новый список в котором содержаться
# элементы из my_list у которых первый символ - буква "a".

def my_def(value):
    new_list = []
    for i in value:
        if 'a' in str(i)[0]:
            new_list.append(i)
    return(new_list)


my_list = ['l;kjadlkjngj', 'asfdmsklgnks', 'fadgadg', 'gadagdag', 'agg', 659]
print(my_def(my_list))

############################################################################################

# 3. Написать функцию которой передается один параметр - список строк my_list.
# Функция возвращает новый список в котором содержаться
# элементы из my_list в которых есть символ - буква "a" на любом месте.

def my_def(value):
    new_list = []
    for i in value:
        if 'a' in str(i):
            new_list.append(i)
    return(new_list)


my_list = ['vhbjhbkh', 'asfdmsklgnks', 'fadgadg', ';mknlknj', 956]
print(my_def(my_list))

############################################################################################

# 4. Написать функцию которой передается один параметр - список строк my_list в
# котором могут быть как строки (type str) так и целые числа (type int).
# Функция возвращает новый список в котором содержаться только строки из my_list.

def my_def(value):
    new_list = []
    for i in value:
        if type(i) == str:
            new_list.append(i)
    return(new_list)


my_list = [1235, 'asfdmsklgnks', 'fadgadg', ';mknlknj', 65656, 'fanfkj222', '6458']
print(my_def(my_list))

############################################################################################

# 5. Написать функцию которой передается один параметр - строка my_str.
# Функция возвращает новый список в котором содержаться те символы из my_str,
# которые встречаются в строке только один раз.

def my_def(value):
    new_list = []
    new_dict = {}
    for i in value:
        if i in new_dict.keys():
            new_dict[i] += 1
        else:
            new_dict[i] = 1
    for i, v in new_dict.items():
        if v == 1:
            new_list.append(i)
    return(new_list)


my_str = 'adfdf7hjibbb'
print(my_def(my_str))

############################################################################################

# 6. Написать функцию которой передается два параметра - две строки.
# Функция возвращает список в который поместить те символы,
# которые есть в обеих строках хотя бы раз.

def my_def(value, value_1):
    return(list(set(value).intersection(set(value_1))))


my_str = 'dsnkjnskjngk,vlz,cvp'
my_str_1 = 'asdkfjkjsnjgvl'
print(my_def(my_str, my_str_1))

############################################################################################

# 7. Написать функцию которой передается два параметра - две строки.
# Функция возвращает список в который поместить те символы, которые есть в обеих строках,
# но в каждой только по одному разу.

def my_def(value, value_1):
    my_dict_1 = {}
    my_dict_2 = {}
    my_list_1 = []
    my_list_2 = []
    for i in value:
        if i in my_dict_1.keys():
            my_dict_1[i] += 1
        else:
            my_dict_1[i] = 1
    for i in value_1:
        if i in my_dict_2.keys():
            my_dict_2[i] += 1
        else:
            my_dict_2[i] = 1
    for i, v in my_dict_1.items():
        if v == 1:
            my_list_1.append(i)
    for i, v in my_dict_2.items():
        if v == 1:
            my_list_2.append(i)
    return(list(set(my_list_1).intersection(set(my_list_2))))


my_str = 'dsnkjnskjngk,vlz,cp'
my_str_1 = 'asdkfjkjsnjgvl'
print(my_def(my_str, my_str_1))

############################################################################################

# 8. Даны списки names и domains (создать самостоятельно).
# Написать функцию для генерирования e-mail в формате:
# фамилия.число_от_100_до_999@строка_букв_длинной_от_5_до_7_символов.домен
# фамилию и домен брать случайным образом из заданных списков переданных в функцию в виде параметров.
# Строку и число генерировать случайным образом.
#
# Пример использования функции:
# names = ["king", "miller", "kean"]
# domains = ["net", "com", "ua"]
# e_mail = create_email(domains, names)
# print(e_mail)
# >>>miller.249@sgdyyur.com

import random
import string


def my_email(name, domain):
    letters = string.ascii_letters
    rand_string = ''.join(random.choice(letters) for i in range(random.randint(5, 7)))
    l = str(name[random.randint(0, len(name)-1)] if len(name) > 0 else '')
    n = str(domain[random.randint(0, len(domain)-1)] if len(domain) > 0 else '')
    if l == '' or n == '':
        my_str = 'not enouph parametrs'
    else:
        my_str = l + '.' + str(random.randint(100, 999)) + '@' + rand_string + '.' + n
    return my_str


names = [123, "Anna", "Artem"]
domains = ["net", "com", "ua"]
print(my_email(names, domains))