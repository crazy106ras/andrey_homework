# 1) Дан список словарей persons в формате [{"name": "John", "age": 15}, ... ,{"name": "Jack", "age": 45}]
# а) Напечатать имя самого молодого человека. Если возраст совпадает - напечатать все имена самых молодых.
# б) Напечатать самое длинное имя. Если длина имени совпадает - напечатать все имена.
# в) Посчитать среднее количество лет всех людей из списка.

#1а)
persons = [
    {"name": "Ann", "age": 21},
    {"name": "John", "age": 15},
    {"name": "Andrey", "age": 29},
    {"name": "Jack", "age": 45}
]
my_list = []
for i in persons:
    my_list.append(i["age"])
for i in persons:
    if i["age"] == min(my_list):
        print(i["name"])

#############################################################

#1б)
my_list = []
for i in persons:
    my_list.append(len(i["name"]))
for i in persons:
    if len(i["name"]) == max(my_list):
        print(i["name"])

#############################################################

#1в)
my_sum = 0
for i in persons:
    my_sum += i["age"]
if not len(persons) ==0:
    result = my_sum / len(persons)
else:
    result = 'persons is empty'
print(result)

#############################################################

# 2) Даны два словаря my_dict_1 и my_dict_2.
# а) Создать список из ключей, которые есть в обоих словарях.
# б) Создать список из ключей, которые есть в первом, но нет во втором словаре.
# в) Создать новый словарь из пар {ключ:значение}, для ключей, которые есть в первом, но нет во втором словаре.
# г) Объединить эти два словаря в новый словарь по правилу:
# если ключ есть только в одном из двух словарей - поместить пару ключ:значение,
# если ключ есть в двух словарях - поместить пару {ключ: [значение_из_первого_словаря, значение_из_второго_словаря]}

#2а)
my_dict_1 = {1: 5, 2: 6, 10: 7, 6: 25}
my_dict_2 = {3: 7, 10: 8, 1: 45, 8: 36}
my_list = []
for key in my_dict_1.keys():
    if key in my_dict_2.keys():
        my_list.append(key)
print(my_list)

# result_intersection = list(set(my_dict_1.keys()).intersection(set(my_dict_2.keys())))
# print(result_intersection)

#############################################################

#2б)
my_list = []
for key in my_dict_1.keys():
    if key not in my_dict_2.keys():
        my_list.append(key)
print(my_list)

# result_difference = list(set(my_dict_1.keys()).difference(set(my_dict_2.keys())))
# print(result_difference)

#############################################################

#2в)
new_dict = {}
for key in my_dict_1.keys():
    if key not in my_dict_2.keys():
        new_dict[key] = my_dict_1.get(key)
print(new_dict)

#############################################################

#3г)
new_dict = {}
for key in my_dict_1.keys():
    if key not in my_dict_2.keys():
        new_dict[key] = my_dict_1.get(key)
    else:
        new_dict[key] = [my_dict_1.get(key), my_dict_2.get(key)]
for key in my_dict_2.keys():
    if key not in my_dict_1.keys():
        new_dict[key] = my_dict_2.get(key)
print(new_dict)