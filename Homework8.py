# 1. В текстовый файл построчно записаны имя и фамилия учащихся класса и их оценки.
#     Вывести на экран всех учащихся, чей средний балл меньше 5, также посчитать и вывести средний балл по классу. Так же,
#     записать в новый файл всех учащихся в формате "Фамилия И.       Ср. балл":
#     Выравнивание колонок - желательно!

import codecs


def name(file_path):
    names = []
    with codecs.open(file_path, encoding='utf-8') as i:
        data = i.readlines()
    for i in data:
        student = i.split()[1] + ' ' + i.split()[0][0] + '.'
        names.append(student)
    return names


def max_str(names):
    max_len = 0
    for i in names:
        if len(i) >= max_len:
            max_len = len(i)
    return max_len


def ball(file_path):
    result = []
    with codecs.open(file_path, encoding='utf-8') as i:
        data = i.readlines()
    for i in data:
        sum = 0
        count = 0
        for k in i.split():
            if k.isdigit():
                sum += int(k)
                count += 1
        if count >0:
            av_balls = round(sum / count,2)
        else:
            av_balls = 0
        result.append(av_balls)
    return result


def min_rate(names, values):
    result_list = []
    result = dict(zip(names, values))
    for i, v in result.items():
        result_list.append({i: v})
        if v < 5:
            print(f'{i} have rating under 5')


def class_rating(value):
    sum = 0
    count = 0
    for i in value:
        sum += i
        count += 1
    if count > 0:
        result = round(sum / count,2)
    else:
        result = 0
    print(f'class rating is {result}')


def new_dict (names, values):
    result_list = []
    result = dict(zip(names, values))
    with open('students_results.txt', 'w') as f:
        for i, v in result.items():
            result_list.append({i:v})
        for i in result_list:
            for n, v in i.items():
                f.write(n.ljust(max_len + 5) + str(v) + '\n')


full_path = '/Users/Andrey/PycharmProjects/pythonProject/Students.txt'
names = name(full_path)
values = ball(full_path)
max_len = max_str(names)
result = new_dict(names, values)
min_rate = min_rate(names, values)
class_rating = class_rating(values)


# 2. Создать текстовый файл, записать в него построчно данные, которые вводит пользователь. Окончанием ввода пусть служит
# пустая строка. Каждая введённая строка, в файле, должна начинаться с новой строки.


file = open('input.txt', 'w')
with open('input.txt', 'a') as f:
    while True:
        my_text = input('enter your str:')
        if my_text == '':
            break
        f.write(my_text + '\n')